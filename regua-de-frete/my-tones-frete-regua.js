
    function reguaFrete(){
    $('body').prepend('<p class="regua-frete">Frete grátis acima de R$250,00</p>');
    $('.regua-frete').css({
        'background' : 'black',
        'color' : 'white',
        'text-align' : 'center'
    });
    $(window).on('orderFormUpdated.vtex', function(evt, orderForm) {
        var value = vtexjs.checkout.orderForm.value;
        if(value>=25000){
            $('.regua-frete').text('Você tem direito a Frete grátis')
        }else{
            $('.regua-frete').text('Frete grátis acima de R$250,00')
        }
      });
    }
    $(window).on('load', function(){
        reguaFrete();
    })